class TicketMailer < ActionMailer::Base
  default from: "9c0281f003904ca193301e7239bcb7d3.protect@whoisguard.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.ticket_mailer.bug_fix.subject
  #
  def bug_fix(ticket)
    @ticket = ticket
    @greeting = "Hi"
    mail to: "andre.bautista.a@gmail.com",
              subject: "#{ticket.title} # #{ticket.id}",
              reply_to: "86fb9e3ec38300f9d456c67fba8d0bd4@inbound.postmarkapp.com"
  end
end
