json.array!(@tickets) do |ticket|
  json.extract! ticket, 
  json.url ticket_url(ticket, format: :json)
end
