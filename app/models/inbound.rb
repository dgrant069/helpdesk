class Inbound < ActiveRecord::Base
  belongs_to :ticket

  def self.create_from_inbound_hook(message)
    id = message["Subject"]
    ticket_id = id[/(?<=#)\d+/]
    self.new(:text => message["TextBody"], :ticket_id => ticket_id.to_i)
  end
end
