class InboundsController < ApplicationController
  skip_before_filter :verify_authenticity_token, only: :create

  def create
    @inbound = Inbound.create_from_inbound_hook(Postmark::Json.decode(request.body.read))

    @inbound.save
    render nothing: true
  end

private
    # Never trust parameters from the scary internet, only allow the white list through.
    def inbound_params
      params.require(:inbound).permit(:text, :user_email, :ticket_id)
    end

end


# @ticket = Ticket.create_from_inbound_hook(Postmark::Json.decode(request.body.read))
#   def self.create_from_inbound_hook(message)
#     self.update(message["Subject"].gsub("Re: ", ""), :reply => message["TextBody"])
#   end
