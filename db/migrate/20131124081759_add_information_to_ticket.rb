class AddInformationToTicket < ActiveRecord::Migration
  def change
    add_column :tickets, :title, :string
    add_column :tickets, :body, :text
  end
end
