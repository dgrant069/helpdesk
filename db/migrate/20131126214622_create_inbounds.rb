class CreateInbounds < ActiveRecord::Migration
  def change
    create_table :inbounds do |t|
      t.string :text
      t.timestamps
    end
  end
end
