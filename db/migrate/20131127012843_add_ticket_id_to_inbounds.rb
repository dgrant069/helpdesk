class AddTicketIdToInbounds < ActiveRecord::Migration
  def change
    add_column :inbounds, :ticket_id, :integer
  end
end
